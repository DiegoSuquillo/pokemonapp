//
//  Pokemon.swift
//  Pokemon
//
//  Created by Diego Suquillo on 6/29/18.
//  Copyright © 2018 Diego Suquillo. All rights reserved.
//

import Foundation

struct Pokemon: Decodable {
    var id: Int
    var order: Int
    var name: String
    var weight: Int
    var height: Int
    var sprites: Sprite
}

struct Sprite: Decodable {
    var defaultSprite: String
    
    enum CodingKeys: String, CodingKey {
        case defaultSprite = "front_default"
    }
}
