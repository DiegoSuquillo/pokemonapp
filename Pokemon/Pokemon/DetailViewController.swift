//
//  DetailViewController.swift
//  Pokemon
//
//  Created by Diego Suquillo on 6/29/18.
//  Copyright © 2018 Diego Suquillo. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var imagePokemon: UIImageView!
    @IBOutlet weak var namePokemon: UILabel!
    @IBOutlet weak var weightPokemon: UILabel!
    @IBOutlet weak var heightPokemon: UILabel!
    
    var pokemon:Pokemon!
    var image:UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        namePokemon.text = pokemon.name
        weightPokemon.text = "\(pokemon.weight)"
        heightPokemon.text = "\(pokemon.height)"
        
        let network = Network()
        network.getImage(pokemon.id) { (image) in
            self.imagePokemon.image = image
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
