//
//  ViewController.swift
//  Pokemon
//
//  Created by Diego Suquillo on 6/27/18.
//  Copyright © 2018 Diego Suquillo. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    var pokemons:[Pokemon] = []
    var selectedIndex:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        let network = Network()
        network.getAllPokemon { (pokemonArray) in
            self.pokemons = pokemonArray.sorted(by: { $0.order < $1.order })
            self.tableView.reloadData()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let pokemon = pokemons[selectedIndex]
        let destination = segue.destination as! DetailViewController;
        destination.pokemon = pokemon
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pokemons.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        let pokemon = pokemons[indexPath.row]
        let text = "\(pokemon.name)"
        cell.textLabel?.text = text
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        performSegue(withIdentifier: "toDetailSegue", sender: self)
    }
}

